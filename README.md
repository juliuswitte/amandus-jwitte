# Overview

Amandus is a simple experimentation suite built on the [dealii
library](http://www.dealii.org). It basically allows the
implementation of a new equation by just providing local integrators
for residuals and matrices. In addition, it has a lot of example
applications. You can find more information in [the documentation generated
with doxygen](http://www.mathsim.eu/~gkanscha/amandus/).

## Structure of Amandus

Amandus consists of two parts: first, there is a framework for implementing
finite element discretizations based on the deal.II finite element library.
This framework is established in the class templates `AmandusApplication` and
`AmandusUMFPACK`, as well as a few files with driver functions like `apps.h`.
The application classes contain all functions for building matrices, computing
right-hand sides and residuals, or writing results in various output formats.
They also contain all the data needed for this except the data vectors describing
solutions and residuals. These are set up in the driver functions since they differ
between different numerical algorithms like linear solvers, Newton-like schemes, and time-stepping methods.

The second part of Amandus is a long list of subdirectories with specific applications.
These contain typically several header files with local integrators and
`.cc` files that can be compiled to obtain an executable program. This file
structure mirrors the structure of code written in Amandus. The application
classes of Amandus provide the technical intermediate layer implementing a generic
finite element program. What is left to the user is:

1. Writing local integrators, which describe the action of a differential operator in weak form on cell or face level. These are inherited from `AmandusIntegrator`
   and provide functions to integrate on cells as well as boundary and interior faces.
   These objects incorporate the 'physics' of a problem.
2. Writing a file which contains a `main()` function and sets up the environment
   like creating a mesh, choosing the right drivers and choosing the numerical
   method for solving the problem.

We recommend looking at the subdirectories `laplace` or `stokes` to get the first idea
of this structure.

# Installing, using and improving Amandus

## Getting started

### Prerequisite - deal.II

Amandus is updated along with the [development version of
deal.II](https://github.com/dealii/dealii). Thus first, make sure to obtain
a recent clone of deal.II and build it with Arpack and UMFPACK enabled. If those libraries are installed in default directories of your system, CMake should detect them, such that building is as simple as
```
git clone https://github.com/dealii/dealii.git dealii_src
mkdir dealii_build
cd dealii_build
cmake -DDEAL_II_WITH_ARPACK=ON -DDEAL_II_WITH_UMFPACK=ON ../dealii_src
make -j <number_of_cpu_cores>
```
Notice that neither Arpack nor UMFPACK are required to build Amandus, but many examples make use of direct solvers (UMFPACK) and all eigenvalue examples are based on Arpack. The deal.II website provides
[more information about installing
Arpack](https://dealii.org/developer/external-libs/arpack.html).

### Downloading Amandus

There are two ways of downloading Amandus: you can simply clone the Git repository on bitbucket.org:
```
git clone https://guidokanschat@bitbucket.org/guidokanschat/amandus.git amandus
```
It clones the remote Git repository into a local directory `amandus`.
If you just want to try it, or if you just want to compute some
results with minimal changes, this may be fine. But you may also want
to start bigger projects at some point, implement new equations, and
even help us improving Amandus. To this end, you will want to fork the original Git repository. Please, find further instructions in the section **Developing Amandus** below.

###  Building Amandus

Amandus uses [cmake](http://www.cmake.org/) for configuration of Makefiles. It supports
in and out of source builds. In-source builds are not recommended if you
plan on updating or developing. Thus, a typical setup runs like this
```
cd amandus
mkdir build
cd build
cmake -DDEAL_II_DIR=</path/to/dealii> ..
make -j <number_of_cpu_cores>
```
where `amandus` is the directory from the previous step and `<path/to/dealii>` is the path to `dealii_build` directory from the **Prerequisites** section (Hint: an absolute path is recommended).
It builds the framework for implementing finite element discretizations, described in the previous section **Structure of Amandus**, and generates the shared library  `libamandus.a`. The ones interested in using Amandus as a shared library for their projects might proceed to **Using Amandus as a library**.

### Included Examples

After building Amandus, the `build` directory contains different directories `<problem>` with names of specific problems, e.g. `laplace`, `maxwell`, `elasticity`, etc.... They include several parameter files `<example>.prm`. In the top-level directory `amandus` of your Amandus clone you find the very same directories, containing both the example's source code `<example>.cc` and the same parameter file `<example>.prm` During CMake configuration all parameter files are copied to their respective example directory in `build`.

Still being in the `build` directory we `make` the example <example> of problem class <problem> as follows
```
make <problem>_<example>
```
It generates an executable `<example>` in the directory `<problem>`. For example, if we would like to run the elasticity example `polynomial_01` it is as simple as
```
make elasticity_polynomial_01
./elasticity/polynomial_01
```

If you are interested in simple modifications, e.g. changing the output format from `gnuplot` to ParaViews `vtu` format, simply modify the parameter file in the build directory. For changes in parameter files to be effective we do not require to re-build the executable, as these are run-time changes. Adding or adjusting functionality to example codes most likely requires a modification of the source code, thus, running `make` again is required to apply changes. You are invited to share your advances with the Amandus community, for details see **Developing Amandus**.

### Using Amandus as a library

If you want to use Amandus as an external dependency for your own code, you
would call `make` in the `amandus_build` directory to build the library.
Optionally you can install it from the build directory with
```
cmake -DCMAKE_INSTALL_PREFIX=/path/to/install/amandus/to .
make install
```

To use Amandus for your own project you can use a setup similar to
the example in `template_cmake` directory which would be compiled as
```
cmake -DAMANDUS_DIR=/path/to/amandus/build/or/install /path/to/your/project/source
make
```

## Developing Amandus

As long as you do not plan to
give back, you can upload the Amandus Git repository wherever you
like. But if you consider joining our community and giving back, it is
recommended, that you

1. create your own account on bitbucket.org
2. fork Amandus by going to the page (https://bitbucket.org/guidokanschat/amandus) and clicking the 'fork' button and
3. clone it to your computer using the command
```
git clone git@bitbucket.org:your_account_name/amandus.git amandus
cd amandus
git remote add upstream https://guidokanschat@bitbucket.org/guidokanschat/amandus.git
```

We will provide
you with improvements, since Amandus is continuously being
developed. Therefore, it is important, that you perform your own
developments on a branch, which you create by
```
git checkout -b my_branch_name
```
If you work on different projects, it is not a bad idea to create a
branch for each of those. At least, keep your commits separated.

Update often and keep your branches up to date!
```
git remote update
```
If you see any development on `upstream/master`, update your fork
and your branches:
```
git checkout master
git rebase upstream/master
git push
git checkout my_branch
git rebase master
git push -f
```
The last command changes history (!!) in your branch repository. Therefore,
there is some danger involved here and you **always** want to make
sure that all your clones are synchronized before doing
this. Otherwise, work may get lost.

## Building a project for Eclipse with CMake

After building and installing go your project folder in the build directory (e.g. /path/to/build/allen_cahn/) and use:

cmake -"GEclipse CDT4 - Unix Makefiles" -DDEAL_II_DIR=/path/to/installed/dealii /path/to/amandus/source/project/.

Note the "." at the end. Note the spelling Eclipse flag.
That command creates a .project file in the build folder that you can import into Eclipse via:
"File">"Import">"Existing Projects Into Workspace">Select the project folder in the build directory > "Finish"
Then you might have to add the path to the deal.II and amandus source, such that Eclipse finds everything:
Right-click the project on the left side>"Properties">"C/C++ Include Path ...">"Add Folder/File"
1) /.../deal.ii/include
2) /.../amandus
After adding these it might be necessary to rerun the Indexer:
Right-click the project on the left side>"Index">"Rebuild

If you want to compile and run in Eclipse then you can set up everything as usual. See deal.II wiki, section Eclipse.

## Note: soon future change

Currently, Amandus uses the builtin `dealii::Vector` and
`dealii::SparseMatrix` objects. These will be replaced by templates
soon, such that parallelization and imported solvers will be possible.

# Legal note

Amandus is published under the [MIT License](./LICENSE.md).

When you commit a pull request to the amandus repository, you publish
your code together with the remaining parts of amandus under this
license. By submitting such a pull request, you certify that you or
the copyright owner agree to publish under this license.
